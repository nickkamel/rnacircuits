#current version doesn't support negati ve weights
#np.random.randint is (inclusive,exclusive)
#np.random.random_integers is (inclusive,inclusive)

import sys
sys.path.insert(0, 'C:\Users\Nick\Documents\Trading')
from EAlib import *
import numpy as np
from collections import defaultdict
import itertools
from ViennaInterface import Ractip,RactipBatch,RactipSingle,ViennaFoldBatch,ViennaCoFoldBatch,ViennaFold,ViennaCoFold,BiFold,SingleFold,IntToAUGC,AUGCToInt
from ViennaInterface import FormComplement,GetValueUnorderKeys,ConvertComponentNameToString,ViennaDistance,GetRzPartOfDimer,GetKeys2DDict,SubstructureDistance
from ViennaInterface import ViennaDistanceBatch,ListToDotBracket, BuildPairsDict, RactipMulticore, RactipMulticoreSingle, Multicore
import math
sys.path.insert(0, 'C:\Users\Nick\Documents\Visual Studio 2015\Projects\EAserver')
from flk import InsertMFoldingResults,InsertDFoldingResults, InsertMisc

class RNANN(individual):
    def __init__(self,evolvedNames,seqLengths):
        individual.__init__(self) 
        self.potentialFitnesses = dict()
        self.seqLengths = seqLengths
        self.evolvedSeqs = dict()
        self.mFoldResults = dict()
        self.dFoldResults = defaultdict(dict)

        self.ks = defaultdict(dict) 

        #Random initialization
        for i,evolvedName in enumerate(evolvedNames):
            self.evolvedSeqs[evolvedName] = self.RandomSeq(seqLengths[i])

        #self.compStem2Start = np.random.randint(0,seqLengths['OBS']-seqLengths['Stem2Bottom'])
        self.compStem2Start = np.random.randint(0,22-5) #!! Hardcoded for now
                              
    def RandomSeq(self,seqLength):
        seq = list()
        for baseId in range(seqLength):
            seq.append(np.random.randint(0,4))
        return seq
 

class RNAEA(EA):
    def __init__(self,designType, designParams,foldingParams):
        EA.__init__(self) #Check if this works. If not use Parent.__init__(self)
        self.designType = designType

        self.R = 1.9872036e-3
        self.T = 37+273.15
        self.INF = 2 #2 because 1/n = 1 when n = 1 so we just need to make 1/0 bigger than that

        #Maps
        self.biasInputPairsNames = list()
        self.rzInputPairs = dict() #Used for weight calculation. Key is a tuple (layerId,neuronId,inputId). Value is (RzName,inputName)
        self.inputShuntPairs = dict() #Used for weight calculation
        self.partsList = dict()
        
        self.foldingParams = foldingParams

        self.evolvedNames = list()

        #We don't consider the parts closing the core as being part of stems
        #Right now, converter template is hardcoded (can't change segment sizes)
        ##CONVERTER TEMPLATE
        self.rzCoreS_12 = AUGCToInt('CUGAUGAG')
        self.rzCoreS23 = AUGCToInt('CGAAA')
        self.rzCoreS3_1 = AUGCToInt('UA')

        self.workingConvStructure = '((((((((.......((((((...........................))))))...(((((....))))).))))))))..............' 
        self.barelyWorkingConv    = [((0,8),(0,73)),  ((0,9),(0,0)), ((0,10),(0,0)), ((0,11),(0,0)), ((0,12),(0,0)), ((0,13),(0,0)), ((0,14),(0,0)), ((0,15),(0,0)), ((0,16),(0,54)), ((0,55),(0,0)), ((0,56),(0,0)), ((0,57),(0,0)), ((0,58),(0,71)),  ((0,72),(0,0))]
        self.compCleavedLenConv   = 8 #Needs to be 8
        self.stem2BottomLen       = 5 #Needs to be 5
        self.segmentLengthsConv   = [self.compCleavedLenConv,8,self.stem2BottomLen,4+22+1,5,5,12,2,22]

        segmentStartId = 0
        self.segmentsBaseIdsConv = list()
        for segmentLength in self.segmentLengthsConv:
            segmentEndId = segmentStartId + segmentLength-1
            self.segmentsBaseIdsConv.append(range(segmentStartId+1,segmentEndId+2))
            segmentStartId = segmentEndId+1

        self.colorStringConv = '0-7:blue 8-15:orange 16-20:#FF1493 25-46:#aec7e8 48-52:#FF1493 53-57:orange 58-69:red 70-71:orange 72-93:green'


        self.stem2BottomLen = 5
        self.stem3BottomLen = 4
        self.stem3HairpinLen = 4
        ##CONVERTER TEMPLATE END

        ##AMPLIFIER TEMPLATE START
        self.ampCore1S_12 = AUGCToInt('CUGAUGAG')  #core1_clv_s2
        self.ampCore1S23 = AUGCToInt('CGAAA')      #core1_s2_s3
        self.ampCore1S3_1 = AUGCToInt('UA')
        self.ampCore2S_12 = AUGCToInt('CUGAUGAG')  #core2_clv_s2
        self.ampCore2S23 = AUGCToInt('CGAAA')
        self.ampCore2S3_1 = AUGCToInt('UA')

        self.inputLen = 22
        self.inputLenConv = 20
        self.compCleavedLen = 10
        self.stem2ABottomLen = 5
        self.preObsLen = 4
        self.obsLen = 22
        self.postObsLen = 1
        self.stem2BBottomLen = 5
        self.stem2BHairpinLen = 4
        self.stem3BBottomLen = 5
        self.stem3BHairpinLen = 4
        self.cleavedLen = 20 #Changed from 22 on April 5

        self.bulgeStart = self.compCleavedLen/2
        self.bulgeEnd   = self.cleavedLen-self.compCleavedLen/2
            

        stem2ATopLen = self.stem2ABottomLen
        stem2BTopLen = self.stem2BBottomLen
        stem3BTopLen = self.stem3BBottomLen


        self.segmentLengths = [self.compCleavedLen, len(self.ampCore1S_12), self.stem2ABottomLen, self.preObsLen + self.obsLen + self.postObsLen, stem2ATopLen, len(self.ampCore1S23),
                                self.compCleavedLen, len(self.ampCore2S_12), self.stem2BBottomLen, self.stem2BHairpinLen , stem2BTopLen, len(self.ampCore2S23), self.stem3BBottomLen, 
                                self.stem3BHairpinLen, stem3BTopLen, len(self.ampCore2S3_1), self.cleavedLen, len(self.ampCore1S3_1),self.cleavedLen,len('UA')]

             
        #Bulge custom lengths
        self.workingAmpStructure  = self.compCleavedLen*'(' + '.......(' + self.stem2ABottomLen*'(' + self.preObsLen*'.' + self.obsLen*'.' + self.postObsLen*'.' + self.stem2ABottomLen*')' + ')...(' + self.compCleavedLen*'(' + '.......('
        self.workingAmpStructure += self.stem2BBottomLen*'(' + '....' + self.stem2BBottomLen*')' + ')...(' + self.stem3BBottomLen*'(' + '....' + self.stem3BBottomLen*')' + ').' + self.compCleavedLen/2*')'                       
        self.workingAmpStructure += (self.cleavedLen-self.compCleavedLen)*'.' + self.compCleavedLen/2*')' + ').' + self.compCleavedLen/2*')' + (self.cleavedLen-self.compCleavedLen)*'.' + self.compCleavedLen/2*')' + '..'                                     
                        
        Y = sum(self.segmentLengths[0:18]) + 1 #sum of lengths of everything from compCleavedSeqA to Core1S3_1 inclusively + 1
        Z = sum(self.segmentLengths[0:5]) + 1 #sum of lengths of everything from compCleavedSeqA to stem2ATopSeq inclusively + 1 
        self.barelyWorkingAmpStructure  =[((0,self.compCleavedLen),(0,Y)),((0,self.compCleavedLen+1),(0,0)), ((0,self.compCleavedLen+2),(0,0)), ((0,self.compCleavedLen+3),(0,0)), ((0,self.compCleavedLen+4),(0,0)), 
                                            ((0,self.compCleavedLen+5),(0,0)),((0,self.compCleavedLen+6),(0,0)), ((0,self.compCleavedLen+7),(0,0)),((0,self.compCleavedLen+8),(0,Z)),((0,Z+1),(0,0)),((0,Z+2),(0,0)),((0,Z+3),(0,0)),((0,Z+4),(0,Y-2)),((0,Y-1),(0,0))]
                                 
                                              
        segmentColors = ['blue','orange','#FF1493','#aec7e8','#FF1493','orange','blue','orange','#FF1493','#FF1493','#FF1493','orange','red','red','red','orange','green','orange','green','green']
        segmentStartId = 0
        self.colorStringAmp = ''
        self.segmentsBaseIdsAmp = list()
        for segmentId in range(len(segmentColors)):
            segmentEndId = segmentStartId + self.segmentLengths[segmentId]-1
            self.colorStringAmp += str(segmentStartId) + '-' + str(segmentEndId) + ':' + segmentColors[segmentId] + ' '
            self.segmentsBaseIdsAmp.append(range(segmentStartId+1,segmentEndId+2))
            segmentStartId = segmentEndId+1
        ##AMPLIFIER TEMPLATE END

        self.inputOBSComp = 1
        self.obsStem2Comp = 1

        if self.designType == 'NN':
            self.targetWeightMatrices = designParams[0]
            self.biases = designParams[1]
            self.evolveShunts = 1
            self.coupledSeqs = []
        elif self.designType == 'Amplifier' or self.designType == 'Amplifier-two-stage' or self.designType == 'Converter': 
            self.coupledSeqs = designParams[0] #coupled sequences is a dictionary of the form coupledName: coupledSeq               
            self.nonLinear = 1
            if self.designType == 'Amplifier' or self.designType == 'Amplifier-two-stage':
                self.identicalCleaved = 1
                if self.designType == 'Amplifier':
                    self.singleStage = 0 #If this is set to 0, it means that we are evolving an amplifier to be used as part of two stage system. This is different from amplifier-two-stage where we are evolving both stages at once
                else:
                    self.singleStage = 0 #For our two stage amplifier this is obviously always 0


        self.colorStrings = {'Amp': self.colorStringAmp, 'Conv': self.colorStringConv, 'Other': ''}
        #self.evolvedLengths  =  {'Input':self.inputLen, 'PreOBS': self.preObsLen, 'OBS': self.obsLen, 'PostOBS': self.postObsLen, 'Stem2ABottom': self.stem2ABottomLen, 'Stem2BBottom': self.stem2BBottomLen, 
        #                         'Stem2BHairpin':self.stem2BHairpinLen, 'Stem3BBottom': self.stem3BBottomLen, 'Stem3BHairpin':self.stem3BHairpinLen, 'Cleaved': self.cleavedLen, 'Stem2Bottom': 5, 'Stem3Bottom': 4, 'Stem3Hairpin':4}
        self.evolvedLengths = dict()
        self.componentNames = list()


        self.JPcutoff = 0      
        self.randomSearchEnabled = 1
        self.localSearchEnabled = 0
        self.localSearchFitnesses = ['JPScore','boundRzScore']
        if self.designType == 'Amplifier-two-stage':
            self.localSearchFitnesses = ['JPScoreMin','boundRzScoreMin']
        self.localSearchProbs = [((-1,2),(-0.1,2),0),((2,100),(-1,0.03),0),((2,100),(0.03,0.06),0.20),((2,100),(0.06,0.12),0.30),((2,100),(0.12,2),0.60)]
        self.randomCutoff = [0]*2 ##!!!!! Hardcoded. Will break if we change the number of selected fitnesses!!!!

        self.currentPartId = 0
        self.partsList = dict()
        self.types = dict()

        self.GenerateComponentNames()  

    def AddComponent(self,name,pointers,type):
        self.componentNames.append(name)
        parts = list()
        for pointer in pointers:
            if pointer[0] == "New":
                self.evolvedNames.append(self.currentPartId)
                self.evolvedLengths[self.currentPartId] = pointer[1]
                parts.append(self.currentPartId)   
                self.currentPartId += 1                
            else:
                parts.append(pointer[1])
            
        self.partsList[name] = parts
        if type in self.types:
            self.types[type].append(name)
        else:
            self.types[type] = [name]


    def AddInput(self, metadata):
        name = ('Input',) + metadata
        self.componentNames.append(name)
        self.evolvedNames.append(name)
        return name


    def AddBias(self, matchingInputName,metadata):
        name = ('Bias',) + metadata
        self.componentNames.append(name)
        self.biasInputPairsNames.append((matchingInputName,name))

    def AddShunt(self, matchingInputName,metadata):
        name = ('Shunt',) + metadata
        self.componentNames.append(name)
        self.evolvedNames.append(name)
        self.inputShuntPairs[metadata] = (matchingInputName,shuntName)

    #The cleavedN is not evolved here
    def AddConverter(self, inputName,cleavedN,metadata):
        name           = ('Conv',) + metadata                
        preObsN        = ('PreOBS',) + metadata
        obsN           = ('OBS',) + metadata
        postObsN       = ('PostOBS',) + metadata
        stem2BottomN   = ('Stem2Bottom',) + metadata
        stem3BottomN   = ('Stem3Bottom',) + metadata
        stem3HairpinN  = ('Stem3Hairpin',) + metadata

        self.componentNames.append(name)
        self.evolvedNames += [preObsN,obsN,postObsN,stem2BottomN,stem3BottomN,stem3HairpinN]
        if self.inputOBSComp == 1: #If we want the OBS to be generated from the input instead of being evolved.
            self.evolvedNames.remove(obsN)
        if self.obsStem2Comp == 1:
            self.evolvedNames.remove(stem2BottomN)

        self.partsList[name]      = (stem2BottomN, preObsN, obsN, postObsN, stem3BottomN, stem3HairpinN, cleavedN) 
        self.rzInputPairs[metadata] = (name,inputName)

    #The cleavedAN and cleavedBN are not evolved here
    def AddAmplifier(self, inputName,cleavedAN,cleavedBN,metadata):
        name           = ('Amp',) + metadata 
        stem2ABottomN  = ('Stem2ABottom',) + metadata 
        preObsN        = ('PreOBS',) + metadata
        obsN           = ('OBS',) + metadata
        postObsN       = ('PostOBS',) + metadata
        stem2BBottomN  = ('Stem2BBottom',) + metadata 
        stem2BHairpinN = ('Stem2BHairpin',) + metadata 
        stem3BBottomN  = ('Stem3BBottom',) + metadata 
        stem3BHairpinN = ('Stem3BHairpin',) + metadata 

        self.componentNames.append(name)
        self.evolvedNames += [stem2ABottomN,preObsN,obsN,postObsN,stem2BBottomN,stem2BHairpinN,stem3BBottomN,stem3BHairpinN]
        if self.inputOBSComp == 1:
            self.evolvedNames.remove(obsN)
        if self.obsStem2Comp == 1:
            self.evolvedNames.remove(stem2ABottomN)
        if self.identicalCleaved == 1: #We want both cleaved sequences to be identical to each other
            self.evolvedNames.remove(cleavedBN)
            self.componentNames.remove(cleavedBN)
        if self.singleStage == 1: #We want both cleaved sequences to be identical to the input. This is the case if we want a single stage amplifier.
            self.evolvedNames.remove(cleavedAN)
                            
        self.partsList[name]      = (stem2ABottomN, preObsN, obsN, postObsN, stem2BBottomN, stem2BHairpinN, stem3BBottomN, stem3BHairpinN, cleavedAN, cleavedBN)      
        self.rzInputPairs[metadata] = (name,inputName)


    #This function generates the names of the evolved sequences and components. 
    #It also generates the blueprint mapping for the ribozyme
    #It also generates the desired intereactions for input-shunt, input-bias, and input-rz
    def GenerateComponentNames(self):
        if self.designType == 'NN': 
            #A layer consists of inputs,biases,OBS in that order
            #The first layer is special: It doesn't have biases
            #The last layer is special. It is an additional virtual layer that doesn't have any OBS
            numLayers = len(self.targetWeightMatrices)
            for layerId,targetWeightMatrix in enumerate(self.targetWeightMatrices): 
                numInputs = np.shape(targetWeightMatrix)[1] #The number of inputs is equal to the number of columns
                numNeurons = np.shape(targetWeightMatrix)[0] #Number neurons is equal to number of rows
                for inputId in range(numInputs):
                    inputName = self.AddInput((layerId,inputId))

                    if (layerId != 0):  #The system inputs have no biases
                        if self.biases[layerId-1][inputId] !=0: #only add bias component if there is an actual bias value
                            self.AddBias(inputName,(layerId-1,inputId)) #The inputs to the current layer are the outputs of the previous layer to which the bias is applied

                    #Add the nth input of each neuron in layer (and their OBS and shunt subcomponents)
                    for neuronId in range(numNeurons):
                        targetWeight = self.targetWeightMatrices[layerId][neuronId][inputId]
                        if targetWeight != 0: #values of 0 in the weight matrix don't require a ribozyme
                            metadata = (layerId,neuronId,inputId)
                            cleavedName = ('Input',layerId+1,neuronId)
                            self.AddConverter(inputName,cleavedName,metadata)

                            if self.evolveShunts == 1:
                                if targetWeight != 1: #values of 1 don't require a shunt
                                    self.AddShunt(inputName,metadata)

            ##Handle the special case of the output layer. We are adding an extra "virtual layer" consisting of only of the inputs and the biases
            outputWeightMatrix = self.targetWeightMatrices[-1]
            numOutputs = np.shape(outputWeightMatrix)[0]
            layerId = len(self.targetWeightMatrices) #We are using 0 based indexing so the last layer treated in the loop is len(self.targetWeightMatrices)-1
            for outputId in range(numOutputs):
                if self.biases[layerId-1][outputId] !=0: #only add bias component if there is an actual bias value
                    self.AddBias(inputName,(layerId-1,outputId))
                self.AddInput((layerId,outputId)) #We are giving it an "input" type, even though it is an output. It is an input to a "virtual layer"

        
        #Here OBS are complementary to the inputs
        #Cleaved are "identical" to the inputs
        #Stem 2 bottom is complementary to OBS
        elif self.designType == 'Amplifier-two-stage': 
            self.AddComponent('InputAmp',[('New',18)],'input')
            self.AddComponent('InputConv',[('New',16)],'input')
            self.AddComponent('Amp',[('New',self.preObsLen),('New',self.postObsLen),('New',self.stem2BBottomLen),
                                     ('New',self.stem2BHairpinLen),('New',self.stem3BBottomLen),('New',self.stem3BHairpinLen),
                                     ('REF',self.partsList['InputConv'][0]),('REF',self.partsList['InputAmp'][0])],'rz')
            self.AddComponent('Conv',[('New',self.preObsLen),('New',self.postObsLen),
                                      ('New',self.stem3BottomLen),('New',self.stem3HairpinLen),
                                      ('REF',self.partsList['InputConv'][0]),('REF',self.partsList['InputAmp'][0])],'rz')
            self.rzInputPairs[0] = ('Amp','InputAmp')
            self.rzInputPairs[1] = ('Conv','InputConv')

        elif self.designType == 'Converter':
            inputName   = self.AddInput((7,7,7))
            cleavedName = self.AddInput((8,8,8))
            self.AddConverter(inputName,cleavedName, (7,7,7))

        '''        
        elif self.designType == 'Amplifier' or self.designType == 'Amplifier-two-stage': #Evolve a 1 or 2 stage amplifier
            inputAmpName = self.AddInput((1,0,0)) #The (1,0,0) is just vestigial
            cleavedAName = self.AddInput((1,1,0))
            cleavedBName = self.AddInput((1,2,0))
            self.AddAmplifier(inputAmpName,cleavedAName,cleavedBName,(1,0,0)) #This evolves the amplifier minus the cleaved parts which are evolved above. However, these may be removed from the evolved list depending on the settings

            if self.designType == 'Amplifier-two-stage':
                self.AddConverter(cleavedAName,inputAmpName,(2,0,0)) #The input to the converter is the output of the amplifier and the output of the converter is the input to the amplifier
        '''

        #self.rzNames = [c for c in self.componentNames if 'Conv' in c[0] or 'Amp' in c[0]]
    
        #Generate pair names
        self.componentPairsNames = list()
        numComponents = len(self.componentNames)
        for i in range(numComponents):
            c1 = self.componentNames[i]
            for j in range(i,numComponents):
                c2 = self.componentNames[j] #we are adopting the convention that the first seq should be the rz 
                if 'Conv' in c2 or 'Amp' in c2:     pair = (c2,c1)
                else:                                    pair = (c1,c2)
                self.componentPairsNames.append(pair)
                
        #Don't evolve any of the segments that have been passed as couplings
        for e in self.evolvedNames:
            if e in self.coupledSeqs.keys():
                self.evolvedNames.remove(e)   

        self.evolvedMutationMap = list()
        for e in self.evolvedNames:
            length = self.evolvedLengths[e]
            for i in range(length):
                self.evolvedMutationMap.append((e,i))
        print self.evolvedMutationMap

    def create_individual(self):
        newIndividual = RNANN(self.evolvedNames,self.evolvedLengths)
        return newIndividual

    def configure_population(self):
        pass


    def GenerateRzSeq(self,ind):
        #Generate full ribozyme sequences
        #The cleaved sequence = the top stem1 closing base + top stem1
        rzSeqs = dict()
        evoCoupSeqs = dict(ind.evolvedSeqs.items() + self.coupledSeqs.items())
        ind.phenoGenoMap = list()
        UA = self.ampCore2S3_1
        CC = AUGCToInt('CC')
        GG = AUGCToInt('GG')
        for rzN in self.types['rz']:
            if 'Conv' in rzN:
                preObsS        = evoCoupSeqs[self.partsList[rzN][0]]
                postObsS       = evoCoupSeqs[self.partsList[rzN][1]]    
                stem3BottomS   = evoCoupSeqs[self.partsList[rzN][2]]
                stem3HairpinS  = evoCoupSeqs[self.partsList[rzN][3]]
                convInput      = GG + evoCoupSeqs[self.partsList[rzN][4]] + CC + UA
                obsS           = FormComplement(convInput)[::-1] 
                cleavedS       = GG + evoCoupSeqs[self.partsList[rzN][5]][0:4]+ CC + evoCoupSeqs[self.partsList[rzN][5]][4:18] #cleavedS = AmpInput

                compCleavedS   = FormComplement(cleavedS[0:self.compCleavedLenConv])[::-1]
                stem3TopS      = FormComplement(stem3BottomS)[::-1]
                stem2BottomS   = FormComplement(obsS)[ind.compStem2Start:ind.compStem2Start+self.stem2BottomLen]              
                stem2TopS      = FormComplement(stem2BottomS)[::-1]

                rzSeqs[rzN] = compCleavedS + self.rzCoreS_12 + stem2BottomS + preObsS + obsS + postObsS + stem2TopS + self.rzCoreS23 + stem3BottomS + stem3HairpinS + stem3TopS + self.rzCoreS3_1 + cleavedS 

                '''
                #This only works for OBS-input and OBS-stem 2 complementary case. Only works when cleaved is not the same as input
                temp = [(self.partsList[rzN][6],range(0,self.compCleavedLenConv)[::-1]),  #comp cleaved 
                        ('None',range(len(self.rzCoreS_12))),                               #ampCore1S_12
                        (inputN,range(ind.compStem2Start,ind.compStem2Start+len(stem2BottomS))),               #stem 2 bottom
                        (self.partsList[rzN][1],range(len(preObsS))),                     #Pre OBS
                        (inputN,range(len(obsS))),                                          #OBS
                        (self.partsList[rzN][3],range(len(postObsS))),                    #Post OBS
                        (inputN,range(ind.compStem2Start,ind.compStem2Start+len(stem2BottomS))[::-1]),        #stem 2 top
                        ('None',range(len(self.rzCoreS23))),
                        (self.partsList[rzN][4],range(len(stem3BottomS))),                #stem 3 bottom
                        (self.partsList[rzN][5],range(len(stem3HairpinS))),               #stem 3 hairpin
                        (self.partsList[rzN][4],range(len(stem3BottomS))[::-1]),          #stem 3 top
                        ('None',range(len(self.rzCoreS3_1))),
                        (self.partsList[rzN][6],range(len(cleavedS)))]                    #cleaved
                '''

            elif 'Amp' in rzN:
                preObsS        = evoCoupSeqs[self.partsList[rzN][0]]
                postObsS       = evoCoupSeqs[self.partsList[rzN][1]]  
                stem2BBottomS  = evoCoupSeqs[self.partsList[rzN][2]]            
                stem2BHairpinS = evoCoupSeqs[self.partsList[rzN][3]]
                stem3BBottomS  = evoCoupSeqs[self.partsList[rzN][4]]            
                stem3BHairpinS = evoCoupSeqs[self.partsList[rzN][5]]   

                stem2BTopS     = FormComplement(stem2BBottomS)[::-1]
                stem3BTopS     = FormComplement(stem3BBottomS)[::-1]
                
                ampInput = GG + evoCoupSeqs[self.partsList[rzN][7]][0:4]+CC + evoCoupSeqs[self.partsList[rzN][7]][4:18]
                obsS = FormComplement(ampInput)[::-1]

                stem2ABottomS  = FormComplement(obsS)[ind.compStem2Start:ind.compStem2Start+self.stem2ABottomLen]
                stem2ATopS     = FormComplement(stem2ABottomS)[::-1]

                cleavedAS       = GG + evoCoupSeqs[self.partsList[rzN][6]] + CC #cleavedA + UA = ConvInput
                cleavedBS = cleavedAS
                
                compCleavedAS = FormComplement(cleavedAS[self.bulgeEnd:self.cleavedLen])[::-1] + FormComplement(cleavedAS[0:self.bulgeStart])[::-1]
                compCleavedBS = FormComplement(cleavedBS[self.bulgeEnd:self.cleavedLen])[::-1] + FormComplement(cleavedBS[0:self.bulgeStart])[::-1]

                rzSeqs[rzN] = compCleavedAS + self.ampCore1S_12 + stem2ABottomS + preObsS + obsS + postObsS + stem2ATopS + self.ampCore1S23 + compCleavedBS +\
                              self.ampCore2S_12 + stem2BBottomS + stem2BHairpinS + stem2BTopS + self.ampCore2S23 + stem3BBottomS + stem3BHairpinS + stem3BTopS +\
                              self.ampCore2S3_1 + cleavedBS + self.ampCore1S3_1 + cleavedAS + UA

                '''
                #This only works for OBS-input and OBS-stem 2 complementary case. Only works when cleaved is not the same as input
                temp = [(self.partsList[rzN][8],range(self.bulgeEnd,self.cleavedLen)[::-1]+range(0,self.bulgeStart)[::-1]), #comp cleaved A 
                        ('None',range(len(self.ampCore1S_12))),                                                               #ampCore1S_12
                        (inputN,range(ind.compStem2Start,ind.compStem2Start+len(stem2ABottomS))),                             #stem 2 bottom A
                        (self.partsList[rzN][1],range(len(preObsS))),                                                       #Pre OBS
                        (inputN,range(len(obsS))),                                                                            #OBS
                        (self.partsList[rzN][3],range(len(postObsS))),                                                      #Post OBS
                        (inputN,range(ind.compStem2Start,ind.compStem2Start+len(stem2ABottomS))[::-1]),                       #stem 2 top A
                        ('None',range(len(self.ampCore1S23))),
                        (self.partsList[rzN][8],range(self.bulgeEnd,self.cleavedLen)[::-1]+range(0,self.bulgeStart)[::-1]), #comp cleaved B
                        ('None',range(len(self.ampCore2S_12))),
                        (self.partsList[rzN][4],range(len(stem2BBottomS))),                                                 #stem 2 bottom B
                        (self.partsList[rzN][5],range(len(stem2BHairpinS))),                                                #stem 2 hairpin B
                        (self.partsList[rzN][4],range(len(stem2BBottomS))[::-1]),                                           #stem 2 top B
                        ('None',range(len(self.ampCore2S23))),
                        (self.partsList[rzN][6],range(len(stem3BBottomS))),                                                 #stem 3 bottom B 
                        (self.partsList[rzN][7],range(len(stem3BHairpinS))),                                                #stem 3 hairpin B
                        (self.partsList[rzN][6],range(len(stem3BBottomS))[::-1]),                                           #stem 3 top B
                        ('None',range(len(self.ampCore2S3_1))),
                        (self.partsList[rzN][8],range(len(cleavedAS))),                                                     #cleaved B
                        ('None',range(len(self.ampCore1S3_1))),
                        (self.partsList[rzN][8],range(len(cleavedAS))),                                                     #cleaved A
                        ('None',[0,1])]                                                                                       #UC

            phenoGenoMap = list()
            for i in temp:
                name    = i[0]
                indices = i[1]
                for j in indices:
                    phenoGenoMap.append((name,j))
            ind.phenoGenoMap.append(phenoGenoMap)
            '''
        
        return rzSeqs


    def generate_semantics_offspring(self):
        #####
        #####PREPARE TASKS
        #####
        dTasks = list()
        mTasks = list()
        for ind in self.offspring: #we only evaluate the offspring because the parents have already been evaluated
            ind.rzSeqs = self.GenerateRzSeq(ind)

            ##!! Generate the input sequences #Added April 6
            ##Current assumes complementary cleaved length of 10
            inputSeqs = dict()
            UA = self.ampCore2S3_1
            CC = AUGCToInt('CC')
            GG = AUGCToInt('GG')
            for inputN in self.types['input']:
                if self.designType == 'Amplifier-two-stage':
                    if 'Conv' in inputN:                
                        inputSeqs[inputN] = GG + ind.evolvedSeqs[self.partsList[inputN][0]] + CC + UA
                    elif 'Amp' in inputN:
                        inputSeqs[inputN] = GG + ind.evolvedSeqs[self.partsList[inputN][0]][0:4] + CC + ind.evolvedSeqs[self.partsList[inputN][0]][4:18]
            ####

            

            #Generate bias sequences
            biasSeqs = dict()
            for p in self.biasInputPairsNames:
                inputName = p[0]
                biasName = p[1]
                biasSeqs[biasName] = FormComplement(ind.evolvedSeqs[inputName])


            #Merge evolved complete seqs (Input,Shunt), generated seqs (Rz and bias) and coupling sequence. Then convert them to AUGC
            ##Note that with current implementation, the OBS seq inadvertently is adding to seqsToFold. However, this is not an issue...
            #...Because only the full component names are used to query the seqsToFold dictionary. So the OBS seqs are never accessed.
            seqsNumerical = dict(inputSeqs.items() + self.coupledSeqs.items() + ind.rzSeqs.items() + biasSeqs.items()) #Changed April 6
            ind.seqsToFold = dict()
            for c,seqNumerical in seqsNumerical.items():
                ind.seqsToFold[c] = IntToAUGC(seqNumerical)


            #######Prepare dimer tasks            
            for p in self.componentPairsNames:
                seq1 = ind.seqsToFold[p[0]]
                seq2 = ind.seqsToFold[p[1]]
                #Use Ractip to CoFold the appropriate Rz,input pairs
                if p in self.rzInputPairs.values():  foldingSoftware = 'Ractip'
                else:                                foldingSoftware = 'Vienna'
                dTasks.append((p,(seq1,seq2),foldingSoftware,ind.id))

            ######Prepare monomer tasks
            for c in self.componentNames:
                seq = ind.seqsToFold[c]
                if c in self.types['rz']:          foldingSoftware = 'Ractip'
                else:                              foldingSoftware = 'Vienna'
                mTasks.append((c,seq,foldingSoftware,ind.id))


        #####EXECUTE TASKS
        poolSize = 8

        dTasksVienna = [d for d in dTasks if d[2]=='Vienna'] #Vienna CoFold
        viennaDInput = [d[1] for d in dTasksVienna]
        viennaDResults = Multicore(viennaDInput,ViennaCoFoldBatch,poolSize)
       
        mTasksVienna = [m for m in mTasks if m[2]=='Vienna'] ###Vienna Fold
        viennaMInput = [m[1] for m in mTasksVienna]
        viennaMResults = Multicore(viennaMInput,ViennaFoldBatch,poolSize)

        dTasksRactip = [d for d in dTasks if d[2]=='Ractip'] #Ractip CoFold
        ractipDResults = Multicore(dTasksRactip,RactipMulticore,poolSize)
        
        mTasksRactip = [m for m in mTasks if m[2]=='Ractip'] ###Ractip Single
        ractipMResults = Multicore(mTasksRactip,RactipMulticoreSingle,poolSize)

        #####
        #####HANDLE OUTPUT
        #####
        allMTasks = mTasksVienna + mTasksRactip
        allMResults = viennaMResults + ractipMResults
        allDTasks = dTasksVienna + dTasksRactip
        allDResults = viennaDResults + ractipDResults

        for indId,ind in enumerate(self.offspring):
            dTasksind = [d for d in allDTasks if d[3] == ind.id]
            dResultsind = [allDResults[dId] for dId,d in enumerate(allDTasks) if d[3] == ind.id]
            for foldId,foldingResult in enumerate(dResultsind):
                componentPairName = dTasksind[foldId][0]
                ind.dFoldResults[componentPairName[0]][componentPairName[1]] = foldingResult
        
            mTasksind = [m for m in allMTasks if m[3] == ind.id]
            mResultsind = [allMResults[mId] for mId,m in enumerate(allMTasks) if m[3] == ind.id]
            for foldId,foldingResult in enumerate(mResultsind):
                componentName = mTasksind[foldId][0]
                ind.mFoldResults[componentName] = foldingResult


        #Calculate all equilibrium Ks
        for ind in self.offspring:
            for seqPairName in self.componentPairsNames: ###
                freeEnergyA  = float(ind.mFoldResults[seqPairName[0]][0][0])
                freeEnergyB  = float(ind.mFoldResults[seqPairName[1]][0][0])
                freeEnergyAB = float(ind.dFoldResults[seqPairName[0]][seqPairName[1]][0][0])

                currentK = math.exp((freeEnergyA+freeEnergyB-freeEnergyAB)/(self.R*self.T))
                ind.ks[seqPairName[0]][seqPairName[1]] = currentK
  

        ###Calculate coarse grain phenotype
        ###!!!! Adding the UA broke this (because the segment length of input is 22 but with the UA we have 24)
        ###!!!TO DO -> only do this if novelty is selected.
        '''
        for ind in self.offspring:
            riboswitches = list()
            for inputRzPairName in self.rzInputPairs.values(): #Beware dict lack of order
                ligandName = inputRzPairName[1]
                rzName = inputRzPairName[0]
            
                boundRzStructureList = GetValueUnorderKeys(ind.dFoldResults,rzName,ligandName)[0][1]
                unboundRzStructure =  ind.mFoldResults[rzName][0][1]
                riboswitches.append((rzName[0],unboundRzStructure,boundRzStructureList))

            ind.coarseGrainPhenotype = self.CalculateCoarseGrainPhenotype(riboswitches)
        '''

    def indToDRow(self,ind):
        rows = list()
        componentPairNames = GetKeys2DDict(ind.dFoldResults)
        for foldId,p in enumerate(componentPairNames):
            row = (foldId, ind.id, p[0], ind.seqsToFold[p[0]], p[1], ind.seqsToFold[p[1]], ind.dFoldResults[p[0]][p[1]][0][2], ind.ks[p[0]][p[1]],self.trialId)
            rows.append(row)
        return rows

    def indToMRow(self,ind):
        rows = list()
        componentNames = ind.mFoldResults.keys()
        for foldId,c in enumerate(componentNames):
            row = (foldId, ind.id,c, ind.seqsToFold[c], ind.mFoldResults[c][0][2], self.trialId)
            rows.append(row)
        return rows

    def InsertGenoPheno(self,popType):
        rowsM = list()
        rowsD = list()
        if   popType == 0: indsToInsert = self.offspring
        elif popType == 1: indsToInsert = self.population
        elif popType == 2: indsToInsert = self.archive
        for ind in indsToInsert:
            rowsD += self.indToDRow(ind)
            rowsM += self.indToMRow(ind)
        
        InsertMFoldingResults(rowsM)
        InsertDFoldingResults(rowsD)
        InsertMisc(self.trialId,self.rzInputPairs)


    def calculatePhenotypeDistanceBatch(self,indPairs):
        allDistances = list()
        for pair in indPairs:
             ind1 = pair[0]
             ind2 = pair[1]
             distance = 0
             for i in range(len(ind1.coarseGrainPhenotype)):
                 diff = np.subtract(ind1.coarseGrainPhenotype[i],ind2.coarseGrainPhenotype[i])
                 temp = np.absolute(diff)
                 temp2 = np.sum(temp)
                 distance += temp2
             allDistances.append(distance)
        return allDistances

    def evaluate_fitness(self,ind): #we want to maximize fitness 
        #Fitness function 0 and 1
        boundDistances = list()
        boundDistancesInverse = list()
        mismatchesAll = list()
        JPDistances = list()
        numObsStem2ChangesAll = list()
        unboundRzScore = 0

        for id,inputRzPairName in enumerate(self.rzInputPairs.values()):
            
            #Bound structure analysis -> We want it to cleave.
            currentRzBoundScore = 0
            ligandName = inputRzPairName[1]
            rzName = inputRzPairName[0]
            
            boundRzStructureList = GetValueUnorderKeys(ind.dFoldResults,rzName,ligandName)[0][1]
            if 'Conv' in rzName:
                boundDistance = self.CalculateBoundDistance(boundRzStructureList,self.workingConvStructure,'Conv')
                m = self.CalculateMismatches(boundRzStructureList,self.workingConvStructure,'Conv')
                mismatchesAll += [(id,i) for i in m]
            elif 'Amp' in rzName:
                boundDistance = self.CalculateBoundDistance(boundRzStructureList,self.workingAmpStructure,'Amp')
                m = self.CalculateMismatches(boundRzStructureList,self.workingAmpStructure,'Amp')
                mismatchesAll += [(id,i) for i in m]


            if boundDistance !=0:
                boundDistanceInverse = 1/float(boundDistance) # we want the bound Rz to be as close as possible to working Rz structure
            else:
                boundDistanceInverse= self.INF
            boundDistances.append(boundDistance)
            boundDistancesInverse.append(boundDistanceInverse)
            #ind.potentialFitnesses['boundRzScore'+str(rzName[1])] = boundDistanceInverse #BROKEN

            #JP score
            #I am putting everything outside the core loop (the tree stems, the OBS, and the hanging strand) as full don't cares.
            #This could be cleaner if I calculated the JP distance the same way I calculated the bound distance:
            #By disconnecting the connecting the don't cares and then comparing to barelyWorkingStructure 
            #The difference between JP score and boundRzScore is that boundRzScore takes into account the stems and not just the core.
            #The difference in stringency between the two scores makes sense because we want the active state to be strongly ON and we don't want the inactive state to be weakly OFF
            #To implement this change for amplifier, I will have to add a new set of don't cares in calculateBoundDistance
            unboundRzStructure =  ind.mFoldResults[rzName][0][1]
            if 'Conv' in rzName:
                JPDistance = SubstructureDistance(unboundRzStructure,self.barelyWorkingConv)
            elif 'Amp' in rzName:
                JPDistance = SubstructureDistance(unboundRzStructure,self.barelyWorkingAmpStructure)

            JPDistances.append(JPDistance)
            #ind.potentialFitnesses['JPScore'+str(rzName[1])] = JPDistance #BROKEN
            
            
            ###!!!! TO CLEAN
            #OBS binding constraint
            if 'Amp' in rzName:
                obsStart = self.segmentsBaseIdsAmp[3][0]
                obsEnd = self.segmentsBaseIdsAmp[3][-1]
            else:
                obsStart = 26
                obsEnd = 47
            boundRzStructure = GetValueUnorderKeys(ind.dFoldResults,rzName,ligandName)[0][1] ##!!!! Update for stage 2

            #Count number of OBS-input pairs bound structure
            obsConnectedBasesBound = boundRzStructure[0][obsStart-1:obsEnd]
            numObsInputPairs = 0
            for obsConnectedBase in obsConnectedBasesBound:
                if obsConnectedBase[0] == 1: #if it is connected to the input strand
                    numObsInputPairs += 1 

            #Count number of internal pairs unbound structure
            obsConnectedBasesUnbound = unboundRzStructure[0][obsStart-1:obsEnd]
            numInternalNonObsPairs = 0
            for obsConnectedBase in obsConnectedBasesUnbound:
                if obsConnectedBase[0] == 0 and obsConnectedBase[1] not in range(obsStart,obsEnd+1) and obsConnectedBase[1] != 0:
                    numInternalNonObsPairs += 1


            #Count number of OBS-Core pairs and OBS-stem2 pairs unbound structure

            if 'Amp' in rzName:
                core1S_12BaseIds = self.segmentsBaseIdsAmp[1]
                core1S23BaseIds = self.segmentsBaseIdsAmp[5]
                core1S3_1BaseIds = self.segmentsBaseIdsAmp[17]
                coreBaseIds = core1S_12BaseIds + core1S23BaseIds + core1S3_1BaseIds
                stem2BaseIds = self.segmentsBaseIdsAmp[2] + self.segmentsBaseIdsAmp[4]
            else:
                coreBaseIds = [8,9,10,11,12,13,14,15,16,54,55,56,57,58,71,72]
                stem2BaseIds = [17,18,19,20,21,49,50,51,52,53]
            numObsCorePairs = 0
        
            numObsStem2Pairs = 0
        
            for obsConnectedBase in obsConnectedBasesUnbound:
                if obsConnectedBase[0] == 0 and obsConnectedBase[1] in coreBaseIds:
                    numObsCorePairs += 1
                if obsConnectedBase[0] == 0 and obsConnectedBase[1] in stem2BaseIds:
                    numObsStem2Pairs += 1

            #Count the number of OBS,OBS-Core, and OBS-Stem2 changes, excluding input binding
            numObsChanges = 0
            numObsCoreChanges = 0
            numObsStem2Changes = 0
            for i in range(len(obsConnectedBasesBound)):
                obsConnectedBaseBound = obsConnectedBasesBound[i]
                obsConnectedBaseUnbound = obsConnectedBasesUnbound[i]
                if obsConnectedBaseBound != obsConnectedBaseUnbound: #If the OBS is now connected to a different base
                    if obsConnectedBaseBound[0] != 1:  #and in the bound case it is not connected to the input strand
                        numObsChanges += 1
                    if obsConnectedBaseUnbound[1] in coreBaseIds: #if the obs was connected to a core base but it is now connected to a different base
                        numObsCoreChanges += 1
                    if obsConnectedBaseUnbound[1] in stem2BaseIds: #if the obs was connected to a stem2 base but it is now connected to a different base
                        numObsStem2Changes += 1
            numObsStem2ChangesAll.append(numObsStem2Changes)
            #####        
        


        ####The code block below will break as soon as we have more than one ribozyme
        #boundUnboundDistance = ViennaDistance(boundRzStructureDbRzOnly,unboundRzStructureDb)

        boundDistanceTot = sum(boundDistances)
        if boundDistanceTot !=0:
            boundDistanceTotInverse = 1/float(boundDistanceTot)
        else:
            boundDistanceTotInverse = self.INF

        ind.potentialFitnesses['boundRzScore'] = boundDistanceTotInverse
        ind.potentialFitnesses['boundRzScoreMin'] = min(boundDistancesInverse) #Judge the group by the worst performing member
        ind.potentialFitnesses['JPScore'] = sum(JPDistances) #We want to maximize the distance between the unbound structure and the working core
        ind.potentialFitnesses['JPScoreMin'] = min(JPDistances)
        ind.potentialFitnesses['obsStem2Changes'] = min(numObsStem2ChangesAll)

        if self.nonLinear == 1: 
            if ind.potentialFitnesses['obsStem2Changes'] == 0:
                ind.boolMurdered                          = 1
                ind.potentialFitnesses['obsStem2Changes'] = 0
                ind.potentialFitnesses['stem2Score'] = 0 
                ind.potentialFitnesses['boundRzScore'] = 0
                ind.potentialFitnesses['JPScore']  = 0
                ind.potentialFitnesses['boundRzScoreMin'] = 0
                ind.potentialFitnesses['JPScoreMin']  = 0
            else:
                ind.boolMurdered                          = 0
        else:
            ind.boolMurdered                          = 0
        if self.JPcutoff == 1:
            if 'JPScore' in ind.potentialFitnesses.keys():
                if ind.potentialFitnesses['JPScore']  >= 4:
                    ind.potentialFitnesses['JPScore']  = 4
            if 'JPScoreMin' in ind.potentialFitnesses.keys():
                if ind.potentialFitnesses['JPScoreMin']  >= 4:
                    ind.potentialFitnesses['JPScoreMin']  = 4

        ind.mismatches = mismatchesAll #The [0] is for testing
       
        #Compare actual and target weights
        #If I set evolveShunts to 0, this block of code breaks
        '''
        weightErrors = list()
        weightKs = list()
        ind.actualWeightMatrices = list()
        for layerId,targetWeightMatrix in enumerate(self.targetWeightMatrices): #list of np matrices
            matrixDims = np.shape(targetWeightMatrix)
            numInputs = matrixDims[1] #numInputs is equal to the number of columns
            numNeurons = matrixDims[0]

            actualWeightMatrix = np.zeros((numNeurons,numInputs))

            for inputId in range(numInputs):
                for neuronId in range(numNeurons):
                    targetWeight = self.targetWeightMatrices[layerId][neuronId][inputId]
                    #values of 0 in the weight matrix don't require a ribozyme/shunt and values of 1 don't require shunt
                    #Therefore, as time goes to infinity, any OBS sequence will provide a weight of 1.
                    #Of course, we time to be quick, but that is problem faced by all weights, not just 1.
                    if targetWeight != 0 and targetWeight != 1:
                        inputRzPairName =  self.rzInputPairs[(layerId,inputId,neuronId)] #was layerId+1 here and below but I removed it
                        inputShuntPairName = self.inputShuntPairs[(layerId,inputId,neuronId)] 
                        inputName = inputRzPairName[1]
                        rzName = inputRzPairName[0]
                        shuntName = inputShuntPairName[1]

                        currentKrz = GetValueUnorderKeys(ind.ks,rzName,inputName)
                        currentKshunt = GetValueUnorderKeys(ind.ks,shuntName,inputName)

                        actualWeight = 1/float((currentKshunt/float(currentKrz))+1)
                        weightError = targetWeight/float(abs(targetWeight-actualWeight))
                        weightErrors.append(weightError)

                        weightKs.append(abs(currentKrz))


                        actualWeightMatrix[neuronId][inputId] = actualWeight
            ind.actualWeightMatrices.append(actualWeightMatrix)

        #Calculate the cross-talk error
        crosstalkKs = list()
        expectedCrossTalkPairs = self.rzInputPairs.values() + self.inputShuntPairs.values() + self.biasInputPairsNames 
        for seqPairName in self.componentPairsNames: #Don't penalize the seqs that should tak to each other
            if seqPairName not in expectedCrossTalkPairs and seqPairName[::-1] not in expectedCrossTalkPairs: ###This fixes any potential lower triangular problem
                crosstalkKs.append(abs(ind.ks[seqPairName[0]][seqPairName[1]]))
        '''
   
    def CalculateBoundDistanceInternal(self,dimerStructure,workingRzStructure,rzType,boolMismatches):
        #The bulk of the work of this function if enforcing "partial don't care" conditions
        #In order for the ribozyme to be in the active state, certain bases must be connected to other specific bases and certain bases must be not connected
        #This specified in the dot bracket structure in self.workingStructureRz
        #However, certain bases are allowed to be not connected, or they can be connected to certain substructures
        #In order to take this into account, we disconnect every base pair that is allowed to be made, but that is not required to be made.
        #We also need to remove the input strand so that we can compare the rz structures

        rzStructure = dimerStructure[0]
        structureDontCare = copy.deepcopy(rzStructure)

        ###Apply partial don't care conditions

        #This makes it OK if the hanging strand bases (or bulge bases) are disconnected or connected to other hanging strand bases (bulge bases respectively)
        if rzType == 'Amp':
            cleavedBStart = sum(self.segmentLengths[0:16])+1
            cleavedBEnd = cleavedBStart+self.segmentLengths[16]-1
            cleavedAStart = sum(self.segmentLengths[0:18])+1
            cleavedAEnd = cleavedAStart+self.segmentLengths[18]-1
            basesHangingA = [(0,i) for i in range(cleavedAStart+self.bulgeStart,cleavedAStart+self.bulgeEnd+1)]
            basesHangingB = [(0,i) for i in range(cleavedBStart+self.bulgeStart,cleavedBStart+self.bulgeEnd)]
            basesHangingAll = [basesHangingA,basesHangingB]
        elif rzType == 'Conv':
            basesHangingA = [(0,i) for i in range(81,95)]
            basesHangingAll = [basesHangingA]

        for basesHanging in basesHangingAll:            
            basesHangingPartners = [rzStructure[i[1]-1] for i in basesHanging]
            for i in range(len(basesHanging)):
                if basesHangingPartners[i] in basesHanging: #if the hanging strand is connected to itself
                    currentBaseId = basesHanging[i][1]
                    structureDontCare[currentBaseId-1] = (0,0)

        #This makes it OK if the OBS bases are disconnected, connected to the input, or connected to other OBS bases
        if rzType == 'Amp':
            obsStart = sum(self.segmentLengths[0:3])+1
            obsEnd = obsStart + self.segmentLengths[3]-1
            basesObs = [(0,i) for i in range(obsStart,obsEnd+1)]
        elif rzType == 'Conv': 
            basesObs = [(0,i) for i in range(22,49)]
        basesObsPartners = [rzStructure[i[1]-1] for i in basesObs]

        for i in range(len(basesObs)):
            if basesObsPartners[i] in basesObs or basesObsPartners[i][0] == 1: #if the obs elements are connected to each other or the input strand
                currentBaseId = basesObs[i][1]
                structureDontCare[currentBaseId-1] = (0,0)


        ###Disconnect input strand

        #These are the ribozyme bases that are connected to the input strand but shouldn't be
        numRemainingStrand1Partners = len([partner for partner in structureDontCare if partner[0] == 1])
        #This ignores every base connected to the input strand, but this is erroneous because if, for example, stem 1 is connected to input then that is an error
        #Vienna distance requires that we disconnect all bases from strand 1, even the ones that are hurting us. 
        #So we keep track of those and add their cost later.

        structureInputDisconnected = list()
        for i,basePartner in enumerate(structureDontCare):
            if basePartner[0] == 1:
                structureDontCare[i] = (0,0)

        structureStem2DisconnectedDb = ListToDotBracket(structureDontCare)
        boundDistance = ViennaDistance(structureStem2DisconnectedDb,workingRzStructure)
        boundDistanceTotal = boundDistance + numRemainingStrand1Partners

        #####
        if boolMismatches == 1:
            mismatchIds = list()
            for i,(a,b) in enumerate(zip(structureStem2DisconnectedDb,workingRzStructure)):
                if a != b:
                    mismatchIds.append(i)
            for i,a in enumerate(structureDontCare):
                if a[0] == 1:
                    mismatchIds.append(i)
            return mismatchIds
        else:
            return boundDistanceTotal

    def CalculateBoundDistance(self,dimerStructure,workingRzStructure,rzType):
        return self.CalculateBoundDistanceInternal(dimerStructure,workingRzStructure,rzType,0)

    def CalculateMismatches(self,dimerStructure,workingRzStructure,rzType):
        return self.CalculateBoundDistanceInternal(dimerStructure,workingRzStructure,rzType,1)

    #meh
    def CalculateStem2Score(self,dimerStructure):

        rzStructure = dimerStructure[0]    
        basesStem2 = [(0,17),(0,18),(0,19),(0,20),(0,21),(0,49),(0,50),(0,51),(0,52),(0,53)]
        basesStem2partners = [rzStructure[i[1]-1] for i in basesStem2]
        stem2Score = 0
        for i in range(len(basesStem2)):
            if basesStem2partners[i] in basesStem2: #if the stem 2 elements are connected to each other
                stem2Score+=1

        return stem2Score

    def CalculateCoarseGrainPhenotype(self,rzs):
        types           = ['Amp', 'Conv']
        segmentsLengths = {'Amp': self.segmentLengths, 'Conv': self.segmentLengthsConv} 
        segmentsBaseIds = [self.segmentsBaseIdsAmp, self.segmentsBaseIdsConv] #segment base ids are 1 indexed
        baseIdSegmentDicts = dict()
        for idx,segmentsBaseIds in enumerate(segmentsBaseIds):
            baseIdSegmentDict = dict()
            for segmentId,segmentBaseIds in enumerate(segmentsBaseIds):
                for baseIdLocal in segmentBaseIds:
                    baseIdSegmentDict[(0,baseIdLocal)] = segmentId
            #Handle unconnected case
            baseIdSegmentDict[(0,0)] = segmentId+1
            #handle the input case
            for baseIdLocal in range(1,self.inputLen+1):
                baseIdSegmentDict[(1,baseIdLocal)] = segmentId+2

            baseIdSegmentDicts[types[idx]] = baseIdSegmentDict

        coarseGrainPhenotype = [[0,0],[0,0]]
        for switchId,rz in enumerate(rzs):
            type                 = rz[0] #Amplifier or converter
            unboundStructureUgly = rz[1]
            boundStructureUgly   = rz[2]
            unboundStructure     = BuildPairsDict(unboundStructureUgly)
            boundStructure       = BuildPairsDict(boundStructureUgly)
            stateStructures      = [unboundStructure, boundStructure]
            baseIdSegmentDict    = baseIdSegmentDicts[type]

            for stateId,stateStructure in enumerate(stateStructures):
                numSegments = len(segmentsLengths[type])
                if stateId == 1:
                    numSegments += 1 #The +1 is for the input in the bound case

                #Initialize data structures
                connectedSegmentsInstances = dict()
                for segment in range(numSegments+1): #This +1 is to account for the disconnected case 
                    connectedSegmentsInstances[segment] = list()
                coarseGrainPhenotype[switchId][stateId] = np.zeros((numSegments,numSegments+1)) #The asymmetrical +1 is to account for the disconnected case 

                for baseIdGlobal in stateStructure.keys(): 
                    #!! This will not iterate through the bases in order (because of the dict). 
                    #Therefore, connectedSegmentsInstances[segment] will contain the correct connecting segments, but not in the correct order.

                    segment = baseIdSegmentDict[baseIdGlobal]
                    connectedBaseIdGlobal = stateStructure[baseIdGlobal]
                    connectedSegmentsInstances[segment].append(baseIdSegmentDict[connectedBaseIdGlobal])                  
                    
                    
                for segmentIdx in range(numSegments):
                    for segmentIdy in range(numSegments+1):
                        if segmentIdy in connectedSegmentsInstances[segmentIdx]:    coarseGrainPhenotype[switchId][stateId][segmentIdx,segmentIdy] = 1
                        else:                                                       coarseGrainPhenotype[switchId][stateId][segmentIdx,segmentIdy] = 0

        coarseGrainFlattened =  [y for x in coarseGrainPhenotype for y in x]
        return coarseGrainFlattened          


    ##!!!! Get rid of murder 
    ##Instead, add a criteria for random search
    ##If that criteria is met, mutate every element of the individual. This is equivalent to trying a new random solution!!!
    def mutate_parent(self,ind):
        seqNames = ind.evolvedSeqs.keys()

        temp = 0
        for lf in self.localSearchFitnesses:
            if lf in self.selectedFitnesses:           temp+=1
        if temp == len(self.localSearchFitnesses):     boolLocalInSelectedFits = 1
        else:                                          boolLocalInSelectedFits = 0

        fits = [ind.potentialFitnesses[f] for f in self.localSearchFitnesses]

        #Determine whether random search will happen
        if self.randomSearchEnabled == 1:
            temp = 0
            #print fits
            for id,f in enumerate(fits):
                if f <=self.randomCutoff[id]:  temp+=1 
            if temp !=0:                       boolDoRandom = 1 #all we need is for one fitness to not exceed the random threshold.
            else:                              boolDoRandom = 0
        else:
            boolDoRandom = 0

        if boolDoRandom == 1:
            print "Doing Random Search"
            for i in range(len(self.evolvedMutationMap)):
                seqToMutateName = self.evolvedMutationMap[i][0]
                seqToMutateIdx  = self.evolvedMutationMap[i][1]
                ind.evolvedSeqs[seqToMutateName][seqToMutateIdx] = np.random.randint(0,4) # 'bit' flip mutation
            ind.compStem2Start = np.random.randint(0,self.obsLen-self.stem2BottomLen)
        else:
            if self.localSearchEnabled == 1 and boolLocalInSelectedFits == 1:
                #print ind.mismatches
                #print ind.phenoGenoMap
                mismatches = [mismatch for mismatch in ind.mismatches if ind.phenoGenoMap[mismatch[0]][mismatch[1]][0] != 'None']
                #print "calculating local search prob"
                #print  fits
                for tup in self.localSearchProbs:
                    #print tup
                    temp = 0
                    for id,f in enumerate(fits):
                        if f > tup[id][0] and f <= tup[id][1]:
                            temp += 1
                        #print temp
                    if temp == len(fits):
                        localSearchProb = tup[-1]
                        break

                ####!!!!!!
                localSearchProb = 0.50
                ####!!!!!!
            else:
                localSearchProb = 0

            #print localSearchProb



            for mutationId in range(self.mutation_rate):
                if np.random.random_sample() < localSearchProb:
                    #print "Doing Local Search"
                    ####
                    #print self.workingAmpStructure
                    inputRzPairName = self.rzInputPairs.values()[0]
                    ligandName = inputRzPairName[1]
                    rzName = inputRzPairName[0]
                    boundRzStructureDB = GetValueUnorderKeys(ind.dFoldResults,rzName,ligandName)[0][2]
                    #print boundRzStructureDB
                    #print ind.mismatches
                    #print [ind.phenoGenoMap[mismatch[0]][mismatch[1]][0] for mismatch in ind.mismatches]
                    #print [ind.phenoGenoMap[mismatch[0]][mismatch[1]][1] for mismatch in ind.mismatches]
                    ####
                    if len(mismatches) > 0:                           
                        sample = np.random.randint(0,len(mismatches))
                        mismatch = mismatches[sample]
                        #print mismatchId
                        seqToMutateName  = ind.phenoGenoMap[mismatch[0]][mismatch[1]][0]
                        seqToMutateIdx  = ind.phenoGenoMap[mismatch[0]][mismatch[1]][1]
                        ind.evolvedSeqs[seqToMutateName][seqToMutateIdx] = np.random.randint(0,4)
                else:    
                    sample = np.random.randint(0,len(self.evolvedMutationMap))
                    seqToMutateName = self.evolvedMutationMap[sample][0]
                    seqToMutateIdx  = self.evolvedMutationMap[sample][1]
                    ind.evolvedSeqs[seqToMutateName][seqToMutateIdx] = np.random.randint(0,4) # 'bit' flip mutation

            ###Mutate the region of the OBS with which stem 2 bottom is complementary.
            #This is done once every round.

            if ind.compStem2Start == 0:                                  ind.compStem2Start += 1
            elif ind.compStem2Start == self.obsLen-self.stem2BottomLen:  ind.compStem2Start -= 1
            else:
                delta =  [-1,1][np.random.randint(0,2)]
                ind.compStem2Start += delta