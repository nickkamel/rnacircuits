import numpy as np
import inspect
from sympy import *
from prettytable import PrettyTable

#Simulation issues when
#1) The ratio is close to 1
#2) The matter and antimatter concentrations are close

#NP dimensions are (rows,columns)

##When specifying a multiplication, specify input Variable name, output variable name, and multiplication ratio amount
##If input name is A and output name is B, call the ribozyme Rab and Rab' (when it is cleaved)
#Convert the input and output names to lowercase and append them to "R" -> inputName.lower()
#Equations are
#[A,Rab],kARab,ARab #association of input
#ARab,kARabr,[A,Rab] #dissociation of input
#ARab,kcRab,ARab'B #cleavage -> makes this a really high value for now with no reverse
#ARab'B,ktRab,[ARab',B] -> #turnover -> makes this a really high value for now with no reverse
#TO DO -> We also have to first create the state vars

#When specifiying an activation function, specify, input variable name, output variable name, and bias value
#TO DO find some notation for specifying the antimatter of "A" like "A_" or something

class ChemicalSystem():
    def __init__(self,inputNames,measuredNames,trials):
        self.abundanceConc = 1000 #TO DO -> Put an appropriate value
        self.reverseReactionRate = 0
        self.rzCleavageRate = 10
        self.rzTurnoverRate = 10
        self.stateNameIndexMap = {}
        self.strVars = []
        self.symVars = []
        self.reactions = []
        self.equaStrs = []
        self.equaSyms = []
        self.initialConc = []
        self.rzBindingRate = 0.1
        self.amRate = self.rzBindingRate*1e7 #1e6 was good, 1e5 started to deteroriate
        self.EvaluateGradient = None
        self.EvaluateJacobian = None

        self.inputNames = inputNames
        self.measuredNames = measuredNames
        self.results = []
        self.trials = trials

        self.verbose = 0

    def AddABBenchmark(self,kAA,kAB,kBB):
        self.strVars = ['AA','AB','BB','A','B']

        self.initialConc = [0]*len(self.strVars)

        self.reactions.append([['A','A'],str(kAA),['AA']])
        self.reactions.append( [['A','B'],str(kAB),['AB']])
        self.reactions.append( [['B','B'],str(kBB),['BB']])
        self.reactions.append([['AA'],str(self.reverseReactionRate),['A','A']])
        self.reactions.append( [['AB'],str(self.reverseReactionRate),['A','B']])
        self.reactions.append([['BB'],str(self.reverseReactionRate),['B','B']])

    def AddMultiplier(self,inputName,outputName,ratio):
        #Update state var names
        rzName = 'R' + inputName.lower() + outputName.lower()
        self.strVars.extend((inputName,outputName,rzName,inputName+rzName,rzName+"_"+outputName,rzName+"_"))
        #self.strVars.extend((inputName,rzName,inputName+rzName))

        self.initialConc = [0,0,self.abundanceConc,0,0,0]
        #self.initialConc = [0,self.abundanceConc,0]

        #update reactions
        if (ratio == 1):
            keff = 10000 #This will be our infinity value. TO DO FIX with the new [R] factor
        else:
            keff = self.reverseReactionRate*ratio/float((1-ratio) * self.abundanceConc)

        self.reactions.append([[inputName,rzName],str(keff),[inputName+rzName]]) #This is the association reaction for the input and the rz
        self.reactions.append([[inputName+rzName],str(self.reverseReactionRate),[inputName,rzName]]) #This is the dissociation reaction for the input and the rz
        self.reactions.append([[inputName+rzName],str(self.rzCleavageRate),[rzName+"_"+outputName]]) #cleavage
        self.reactions.append([[rzName+"_"+outputName],str(self.rzTurnoverRate),[rzName+"_",outputName]]) #turnover

    def AddDotProduct(self,inputNames,outputName,ratios,rzConc):
        self.initialConc = [rzConc] + 4*len(inputNames)*[0] + [0]
        self.strVars.extend(('R',outputName))
        for inputId,inputName in enumerate(inputNames):
            if (ratios[inputId] == 1):
                keff = 10000 #This will be our infinity value. TO DO FIX with the new [R] factor
            else:
                keff = self.reverseReactionRate*ratios[inputId]/float((1-ratios[inputId]) * rzConc)
            self.reactions.append([[inputName,'R'],str(keff),["R"+inputName]])
            self.reactions.append([["R"+inputName],str(self.reverseReactionRate),[inputName,'R']])
            self.reactions.append([["R"+inputName],str(self.rzCleavageRate),["R"+inputName+"_"+outputName]]) #cleavage
            self.reactions.append([["R"+inputName+"_"+outputName],str(self.rzTurnoverRate),["R"+inputName+"_",outputName]])
            self.strVars.extend((inputName,"R"+inputName,"R"+inputName+"_"+outputName,"R"+inputName+"_"))

    def AddShuntMultiplier(self,inputName,outputName,ratio):

        ratio = abs(ratio)
        #Update state var names
        rzName = 'R' + inputName.lower() + outputName.lower()
        shName = 'S' + inputName.lower() + outputName.lower() #Instead of outputName this should ideally be neuron name but I'm using outputName as the de facto neuron name
        if (inputName not in self.strVars): #This is to handle later layers of neurons
            self.strVars.append(inputName)
            self.initialConc.append(0)
        self.strVars.extend((rzName,inputName+rzName,rzName+"_"+outputName,rzName+"_",shName,shName+inputName))

        #self.initialConc.extend((0,self.abundanceConc,0,0,0,self.abundanceConc,0))
        self.initialConc.extend((self.abundanceConc,0,0,0,self.abundanceConc,0))

        #update reactions
        if (ratio == 0):
            kSh = 1000 #This is my infinity value
        else:
            kSh = (1-ratio)*self.rzBindingRate/float(ratio)

        self.reactions.append([[inputName,rzName],str(self.rzBindingRate),[inputName+rzName]]) #This is the association reaction for the input and the rz
        self.reactions.append([[inputName+rzName],str(self.reverseReactionRate),[inputName,rzName]]) #This is the dissociation reaction for the input and the rz
        self.reactions.append([[inputName+rzName],str(self.rzCleavageRate),[rzName+"_"+outputName]]) #cleavage
        self.reactions.append([[rzName+"_"+outputName],str(self.rzTurnoverRate),[rzName+"_",outputName]]) #turnover

        self.reactions.append([[inputName,shName],str(kSh),[shName+inputName]]) #The shunt
        #self.reactions.append([[shName + inputName],str(shuntRate/float(10000)),[inputName,shName]]) #The shunt reverse reaction
        #Note that like in the case of the cleavage rate messing up the original multiplier, the shunt reverse reaction has the same effect here.
        #However, it is slow effect that can be ignored.

    def AddDotProductShunt(self,inputNames,outputName,ratios):
        if (outputName not in self.strVars):
            self.strVars.append(outputName)
            self.initialConc.append(0)

        for inputId,inputName in enumerate(inputNames):
            if (ratios[inputId] >= 0):
                self.AddShuntMultiplier(inputName,outputName,ratios[inputId])
            else:
                amName = "M"+outputName.lower()
                self.AddShuntMultiplier(inputName,amName,ratios[inputId])

    def AddAntimatter(self,inputName,antimatterConc):
        amName = "M"+inputName.lower()
        if (amName not in self.strVars):
            self.strVars.extend((amName,amName+inputName))
            self.initialConc.extend((antimatterConc,0))

        self.reactions.append([[inputName,amName],str(self.amRate),[amName+inputName]])

    def AddNeuron(self,inputNames,outputName,ratios,bias):
        self.AddDotProductShunt(inputNames,outputName,ratios)
        self.AddAntimatter(outputName,bias)

    def ConvertStrVarsToSymVars(self):
        self.symVars = len(self.strVars)*[""]
        for varId,strVar in enumerate(self.strVars):
            self.symVars[varId] = sympify(strVar)

        self.stateNameIndexMap = {k: v for v, k in enumerate(self.strVars)}


    def ConvertReactionsToEqua(self):
        self.equaStrs = len(self.strVars)*[""]
        self.equaSyms = len(self.strVars)*[""]

        for reaction in self.reactions:
            reactantMul = ""
            for reactant in reaction[0]:
                reactantMul += "*" + reactant

            for reactant in reaction[0]:
                self.equaStrs[self.stateNameIndexMap[reactant]] += "-" + reaction[1] + reactantMul
            for product in reaction[2]:
                self.equaStrs[self.stateNameIndexMap[product]] += "+" + reaction[1]+ reactantMul

        for equaId, equaStr in enumerate(self.equaStrs):
            if (equaStr != ""): #SymPy can't convert empty strings. If I do things properly, I won't have any empty strings, but I'm keeping this just in case.
                self.equaSyms[equaId] = sympify(equaStr)
        if (self.verbose == 1):
            print self.reactions
            print self.equaSyms



    def FormGradientJacobianEquation(self):
        equaSymsMat = Matrix(self.equaSyms)
        #print equaSymsMat
        gradientDirections = Matrix(self.symVars)
        #print gradientDirections
        jacobianSym = equaSymsMat.jacobian(gradientDirections)
        #print jacobianSym

        array2mat = [{'MutableDenseMatrix': np.matrix(None,dtype = 'float64')}, 'numpy']
        self.EvaluateGradient = lambdify((self.symVars), equaSymsMat, array2mat)
        #print inspect.getsource(self.EvaluateGradient)

        array2mat = [{'MutableDenseMatrix': np.matrix(None,dtype = 'float64')}, 'numpy']
        self.EvaluateJacobian = lambdify((self.symVars), jacobianSym, array2mat)

    def SetInitialValue(self,stateName,value):
        self.initialConc[self.stateNameIndexMap[stateName]] = value

    def SetInitialValues(self,stateNames,values):
        for stateId,stateName in enumerate(stateNames):
            self.SetInitialValue(stateName,values[stateId])

    def GetFinalValue(self,stateName):
        return self.finalState[self.stateNameIndexMap[stateName]][0] #if I don't have the [0] it returns a np array

    def ConfigureSystem(self):
        self.ConvertStrVarsToSymVars()
        self.ConvertReactionsToEqua()
        self.FormGradientJacobianEquation()


    def RunSim(self,duration,h,tol,SStol):

        self.x_SUPER_0 = np.swapaxes(np.array([self.initialConc], dtype = 'float64'),0,1) #convert list to my ugly 2d numpy vector
        numStates = np.shape(self.x_SUPER_0)[0]
        numSteps = int(duration/h)
        stateTrajectory = np.zeros((numStates,numSteps), dtype = 'float64') #each column represents the state vector at a certain time point.

        #x_SUPER_n
        #xDot_SUPER_np1_SUB_i
        #xDot_SUPER_n
        #x_SUPER_np1_SUB_i
        #x_SUPER_np1_SUB_ip1
        #J_SUPER_np1_SUB_i

        x_SUPER_n = self.x_SUPER_0
        
        for stepId in range(numSteps):
            if ((stepId % 100) == 0 and self.verbose == 1):
                print "Current step is "
                print stepId

            xDot_SUPER_n = self.EvaluateGradient(*x_SUPER_n[:,0])
            x_SUPER_np1_SUB_i = x_SUPER_n+h*xDot_SUPER_n #FE to get starting point for newton iteration

            newtonErr = 10000
            newtonCount = 0

            while(newtonErr > tol):   
                #print "Newtown iteration is"
                #print newtonCount

                #xDot_SUPER_np1_SUB_i = CalculateGradient(systemParams,x_SUPER_np1_SUB_i)
                #J_SUPER_np1_SUB_i = CalculateJacobian(systemParams,x_SUPER_np1_SUB_i)
            
                xDot_SUPER_np1_SUB_i = self.EvaluateGradient(*x_SUPER_np1_SUB_i[:,0])
                J_SUPER_np1_SUB_i = self.EvaluateJacobian(*x_SUPER_np1_SUB_i[:,0])

                toInvert = h*J_SUPER_np1_SUB_i-np.eye(numStates, dtype = 'float64')
                toMultiplyByInv = x_SUPER_n+h*xDot_SUPER_np1_SUB_i-x_SUPER_np1_SUB_i

                x_SUPER_np1_SUB_ip1 = x_SUPER_np1_SUB_i - np.matmul(np.linalg.inv(toInvert),toMultiplyByInv)
       
                newtonErr = np.amax(np.absolute(x_SUPER_np1_SUB_i -x_SUPER_np1_SUB_ip1))
                x_SUPER_np1_SUB_i = x_SUPER_np1_SUB_ip1
                newtonCount = newtonCount + 1;

            SSvariation = np.amax(np.absolute(x_SUPER_n -x_SUPER_np1_SUB_ip1))

            x_SUPER_n = x_SUPER_np1_SUB_ip1
            #print x_SUPER_n
            stateTrajectory[:,stepId] = x_SUPER_n[0]

            if (SSvariation < SStol):
                break


        self.finalState = x_SUPER_n
        return self.finalState

    def RunTrials(self):
        for trial in self.trials:
            self.SetInitialValues(self.inputNames,trial)
            self.RunSim(300,2e-4,1e-3,1e-7)
            currentResults = []
            for measuredName in measuredNames:                
                currentResults.append(self.GetFinalValue(measuredName))
            self.results.append(currentResults)

    def PrintResults(self):
        tableHeader = inputNames + measuredNames
        truthTable = PrettyTable(tableHeader)
        for rowId,trial in enumerate(self.trials):
            truthTable.add_row(trial + self.results[rowId])
        print truthTable


#####

### Simulate AND gate
inputNames = ['A','B']
outputNames = ['G']
measuredNames = outputNames
trials = [[0,0],[0,1],[1,0],[1,1]]
andGate = ChemicalSystem(inputNames,measuredNames, trials)
andGate.AddNeuron(inputNames,outputNames[0],[1,1],1.5)
andGate.ConfigureSystem()
andGate.RunTrials()
print "AND gate"
andGate.PrintResults()

### Simulate OR gate
inputNames = ['A','B']
outputNames = ['G']
measuredNames = outputNames
trials = [[0,0],[0,1],[1,0],[1,1]]
orGate = ChemicalSystem(inputNames,measuredNames, trials)
orGate.AddNeuron(inputNames,outputNames[0],[1,1],0.5)
orGate.ConfigureSystem()
orGate.RunTrials()
print "OR gate"
orGate.PrintResults()


### Simulate general neuron
inputNames = ['A','B','C','D']
outputNames = ['F']
measuredNames = outputNames
trials = [[1,1,1,1],[8,6,4,2]]
generalNeuron = ChemicalSystem(inputNames,measuredNames, trials)
#generalNeuron.AddNeuron(inputNames,outputNames[0],[0.2,0.4,0.6,0.8],0)
generalNeuron.AddNeuron(inputNames,outputNames[0],[0.5,-0.1,0.5,-0.1],0)
generalNeuron.ConfigureSystem()
generalNeuron.RunTrials()
print "General neuron"
generalNeuron.PrintResults()



### Simulate XOR
inputNames = ['A','B']
intermediateNames = ['C','G']
outputNames = ['H']
measuredNames = intermediateNames+outputNames
trials = [[0,0],[0,1],[1,0],[1,1]]
xorGate = ChemicalSystem(inputNames,measuredNames, trials)
xorGate.AddNeuron(inputNames,intermediateNames[0],[1,1],0.25)
xorGate.AddNeuron(inputNames,intermediateNames[1],[1,1],0.75)
xorGate.AddNeuron(intermediateNames,outputNames[0],[0.33,-1],0)
xorGate.ConfigureSystem()
xorGate.RunTrials()
print "XOR"
xorGate.PrintResults()



#1e-4,1e-9
#0.06,0.8
#0.002,2