import subprocess
import os, copy
from multiprocessing import Pool

#The way the output is parsed is no longer correct
def BiFold(seq1,seq2,numConformations):

    #generate input fasta files
    seq1FileName = "C:\RNAstructure\\seq1.fasta"
    outputFile = open(seq1FileName,'w+')
    outputFile.writelines('>SEQ1\n')
    outputFile.writelines(seq1)
    outputFile.close()
    seq2FileName = "C:\RNAstructure\\seq2.fasta"
    outputFile = open(seq2FileName,'w+')
    outputFile.writelines('>SEQ2\n')
    outputFile.writelines(seq2)
    outputFile.close()

    ###Perform folding
    resultsFile = "C:\RNAstructure\\results.ct" #The same file is used and will be overwritten each time. ?Potential disk bottleneck?
    inputdata = '"C:\RNAstructure\exe\Bifold.exe" C:/RNAstructure/seq1.fasta C:/RNAstructure/seq2.fasta ' + resultsFile + ' -m ' + str(numConformations)
    os.system(inputdata)

    return ParseRNAStructureResults(resultsFile)

def SingleFold(seq):

    #generate input fasta files
    seqFileName = "C:\RNAstructure\\seq.fasta"
    outputFile = open(seqFileName,'w+')
    outputFile.writelines('>SEQ\n')
    outputFile.writelines(seq)
    outputFile.close()

    ###Perform folding
    resultsFile = "C:\RNAstructure\\results.ct" #The same file is used and will be overwritten each time. ?Potential disk bottleneck?
    inputdata = '"C:\RNAstructure\exe\Fold.exe" C:/RNAstructure/seq.fasta ' + resultsFile
    os.system(inputdata)

    return ParseRNAStructureResults(resultsFile)

def ParseRNAStructureResults(resultsFile):
    ###Parse result file
    numCols = 6
    inputfile = open(resultsFile)
    freeEnergies = list()
    conformations = list()
    for lineId,line in enumerate(inputfile):
        split_line = line.split()
        #print split_line


        if ("ENERGY" in split_line) or (lineId == 0): #This is the beginning of a new conformation so create the list structure to accomodate it
            if "ENERGY" in split_line: 
                freeEnergies.append(float(split_line[3]))
            else:
                freeEnergies.append(0) #To accomodate the pathological case where no free energy shows up
            #if conformations != []:
                #print conformations[-1]
            #print "New sequence"
            conformations.append([])
            for colId in range(numCols):
                conformations[-1].append([])
        else:
            for colId,col in enumerate(conformations[-1]):
                col.append(split_line[colId])


    #print conformations[-1] #for each conformation, the elements of the same column are put in the same list

    secondaryStructures = list()
    dotBrackets = list()
    for conformation in conformations:
        #print "NEW conformation"
        secondaryStructure = []
        numRows = len(conformation[0])
        for rowId in range(numRows):
            if conformation[1][rowId] !="I": #These Is are placeholders
                pair = (int(conformation[0][rowId]),int(conformation[4][rowId])) #the pairs are given in cols 0 and 4
                secondaryStructure.append((0,pair[1])) #This is a hack to get SingleFold working (but it breaks Bifold). 0 is the strandId since it is a single strand.
                #secondaryStructure.append(pair)
        #secondaryStructures.append(secondaryStructure)
        secondaryStructures.append([secondaryStructure]) #This is to fix singlefold since the EA expects a list of strands
        dotBracket = ListToDotBracket(secondaryStructure)
        dotBrackets.append(dotBracket)
    #print secondaryStructures
    #print freeEnergies

    return zip(freeEnergies,secondaryStructures,dotBrackets)

def IntToAUGC(seqInt):
    a = dict()
    a[0] = 'A'
    a[1] = 'U'
    a[2] = 'G'
    a[3] = 'C'
    seqAUGC = ''
    for bpInt in seqInt:
        seqAUGC += a[bpInt]
    return seqAUGC

def AUGCToInt(seqAUGC):
    a = dict()
    a['A'] = 0
    a['U'] = 1
    a['G'] = 2
    a['C'] = 3
    seqInt = list()
    for bpAUGC in seqAUGC:
        seqInt.append(a[bpAUGC])
    return seqInt

def FormComplement(seqInt):
    complement = list()
    for bpId in range(len(seqInt)):
        if seqInt[bpId] == 0:
            complement.append(1)
        elif seqInt[bpId] == 1:
            complement.append(0)
        elif seqInt[bpId] == 2:
            complement.append(3)
        elif seqInt[bpId] == 3:
             complement.append(2)
    return complement

#This returns None if neither order is present
def GetValueUnorderKeys(queriedDict,key1,key2):
    if key2 in queriedDict[key1]:
        return queriedDict[key1][key2]
    elif key1 in queriedDict[key2]:
        return queriedDict[key2][key1]

def GetKeys2DDict(dict2d):
    keys2d = list()
    firstDimKeys = dict2d.keys()
    for firstDimKey in firstDimKeys:
        secondDimKeys = dict2d[firstDimKey].keys()
        for secondDimKey in secondDimKeys:
            keys2d.append((firstDimKey,secondDimKey))
    return keys2d

#Currently only works for single strand
def ListToDotBracket(connectedBaseNames):
    dotBracket = ''
    for baseId,connectedBaseName in enumerate(connectedBaseNames):
        connectedBaseId = connectedBaseName[1]
        if connectedBaseId == 0:
            dotBracket += '.'
        else:
            if connectedBaseId > baseId:
                dotBracket += '('
            else:
                dotBracket += ')'
    return dotBracket

##it would have been much smarter to pass the connected structure and then only take the first(rz) strand and then replacing 
##every base that is connected to strand 1 by (0,0). Then convert that to dot bracket
## This can only parse the first half. Not the second half. 
## For Vienna CoFold, we only have ( and ), so the bases connected to other strand will be the ( without corresponding )
## For Ractip, the bases connected to other strand are given by [
def GetRzPartOfDimer(dimerStructure):
    firstHalfStructure = dimerStructure.split('&')[0] #Above we adopted convention that the Rz will always be the first strand
    boundRzStructureDbRzOnly = ''

    #Now we have to deal with parenthesises that are closed by connecting to the second strand
    waitingToBePaired = list()
    for baseId,baseStructure in enumerate(firstHalfStructure):
        if baseStructure == "(":
            waitingToBePaired.append(baseId)
        elif baseStructure == ")":
            waitingToBePaired.pop()
    for baseId,baseStructure in enumerate(firstHalfStructure):
        if baseId in waitingToBePaired:
            boundRzStructureDbRzOnly += '.' #If the parenthesis is not closed, replace it with a dot
        elif baseStructure == '[':
            boundRzStructureDbRzOnly += '.'
        else:
            boundRzStructureDbRzOnly += baseStructure
    return boundRzStructureDbRzOnly

def BuildPairsDict(uglyStructure): 
    pairsDict = dict()
    for strandId,strand in enumerate(uglyStructure):
        for baseIdLocal0,connectedBaseIdGlobal in enumerate(strand):
            pairsDict[(strandId,baseIdLocal0+1)] = connectedBaseIdGlobal
    return pairsDict

###RNA sequences indexing starts at 1
def ParseDotBracket(dotBracket):
    strands = dotBracket.split('&')
    numStrands = len(strands)
    strandNames = ['A','B'] 

    pairs = list()
    waitingToBePaired = list()
    waitingToBePairedPseudo = list()
    for strandId,strand in enumerate(strands):
        for baseId,baseStructure in enumerate(strand):
            #baseName = strandNames[strandId]+str(baseId+1)
            baseName = (strandId,baseId+1)
            if baseStructure == "(":
                waitingToBePaired.append(baseName)
            elif baseStructure == ")":
                pairs.append((waitingToBePaired.pop(),baseName))
            elif baseStructure == "[":
                waitingToBePairedPseudo.append(baseName)
            elif baseStructure == "]":
                pairs.append((waitingToBePairedPseudo.pop(),baseName))
            elif baseStructure == ".":
                #pairs.append(('',baseName)) #This is if we use the A80 format instead of (0.80) as base name 
                #!!!!!!!!!! Changed this from (strandId,0) to (0,0) if it is not connected. Keep on eye out to see if this breaks anything
                #pairs.append(((strandId,0),baseName)) #0 is the convention that RNAstructure used for unbound bases
                pairs.append(((0,0),baseName))

    #print pairs
    #return pairs #No duplicate pairs are returned

    #Duplicate pairs are return and the order is clean
    strandsConnections = [[] for x in xrange(0,numStrands)]
    test = list()
    for strandId,strandConnections in enumerate(strandsConnections):
        for baseId in range(len(strands[strandId])):
            #baseName = strandNames[strandId]+str(baseId+1)
            baseName = (strandId,baseId+1)
            basePair = [v for v in pairs if v[0] == baseName or v[1] == baseName][0]
            if basePair[0] == baseName:
                connectedBase = basePair[1]
            else:
                connectedBase = basePair[0]
            strandConnections.append(connectedBase)
            test.append(connectedBase)
    return strandsConnections

###Not being used
def GetIntramolecularPairs(dimerStru,targetStru):
    monomerStru = list()
    if targetStru == 0:
        for pair in dimerStru:
            if "B" not in pair[0] and "B" not in pair[1]:
                newPair = (pair[0].replace("A",""),pair[1].replace("A",""))
                monomerStru.append(newPair)
    elif targetStru == 1:
        for pair in dimerStru:
            if "A" not in pair[0] and "A" not in pair[1]:
                newPair = (pair[0].replace("B",""),pair[1].replace("B",""))
                monomerStru.append(newPair)
    return monomerStru

#This only makes sense for NN
def ConvertComponentNameToString(componentName):
    componentType = componentName[0]
    componentLayer = componentName[1]
    stringName = componentType + '_L' + str(componentLayer)
    if componentType == 'Input':
        stringName += 'I' + str(componentName[2])
    elif componentType == 'Bias':
        stringName += 'N' + str(componentName[2])
    else:
        stringName += 'N' + str(componentName[2]) + 'M' + str(componentName[3])
    return stringName

def Tup2Str(tup):
    return ''.join(map(str, tup))

#actual is of the form [strand1,strand2] where strandN = [(connectedStrandId,connectedBaseId),(connectedStrandId,connectedBaseId)]
#target is of the form [((baseStrandId,baseId),(connectedStrandId,connectedBaseId)),(())]
def SubstructureDistance(actual,target):
    #print "actual"
    #print actual
    #print "target"
    #print target
    errorCount = 0
    for targetElem in target:
        strandId = targetElem[0][0]
        baseId = targetElem[0][1]
        connectedStrandIdTarget = targetElem[1][0]
        connectedBaseIdTarget = targetElem[1][1]
        connectedStrandIdActual = actual[strandId][baseId-1][0] #bp indexing starts at 1
        connectedBaseIdActual = actual[strandId][baseId-1][1]
        if connectedStrandIdActual != connectedStrandIdTarget or connectedBaseIdActual != connectedBaseIdTarget:
            errorCount+=1
    return errorCount

def ViennaDistance(structure1,structure2):
    inputData = structure1 + '\n' + structure2
    p1 = subprocess.Popen(["C:\Vienna\RNAdistance.exe"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    output = p1.communicate(input=inputData)[0]
    result_rnadistance = float(output.split(' ')[1])
    return result_rnadistance

def ViennaFold(seq):
    p1 = subprocess.Popen(["C:\Vienna\RNAfold.exe"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    result_rnafold = p1.communicate(input=seq)[0]
    line = result_rnafold.split('\n')[1]
    return ParseViennaResults(line)

def ViennaCoFold(seq1,seq2):
    inputdata=seq1 + "&" + seq2
    p2 = subprocess.Popen(["C:\Vienna\RNAcofold.exe"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    result_rnacofold = p2.communicate(input=inputdata)[0]
    line = result_rnacofold.split('\n')[1]
    return ParseViennaResults(line)

def Ractip(seq1,seq2,params):

    #p1 = subprocess.Popen(["C:/ractip/ractip.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    #p1 = subprocess.Popen(["C:/ractip/ractip.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e","--acc-num=0","--min-w=0","--acc-max-ss","--max-w=100","-t0.001","-a50"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    ractipCommands = ["C:/ractip/ractip.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e","--acc-num="+str(params[4]),
                           "--min-w=0","--acc-max-ss","--max-w=100","-t"+ str(params[2]),"-a"+str(params[0]),"-b"+str(params[1]),"-u"+str(params[3])]
    p1 = subprocess.Popen(ractipCommands,stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    inputdata= seq1 +"\n" + seq2
    result_rnafold = p1.communicate(input=inputdata)[0]
    result_lines = result_rnafold.splitlines()
    db1 = result_lines[2]
    db2 = result_lines[5]
    dbDimer = db1 + "&" + db2
    struDimer = ParseDotBracket(dbDimer)
    freeEnergy = result_lines[6]
    return [(float(freeEnergy),struDimer,dbDimer)] #Added the list brackets to make it consisent with RNAStructure which returns a list of conformations  

def RactipSingle(seq,params):
    #p1 = subprocess.Popen(["C:/ractip/ractip.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    #p1 = subprocess.Popen(["C:/ractip/ractip.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e","--acc-num=0","--min-w=0","--acc-max-ss","--max-w=100","-t0.001","-a50"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    ractipCommands = ["C:/ractip/ractip.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e","--acc-num="+str(params[4]),
                           "--min-w=0","--acc-max-ss","--max-w=100","-t"+ str(params[2]),"-a"+str(params[0]),"-b"+str(params[1]),"-u"+str(params[3])]
    p1 = subprocess.Popen(ractipCommands,stdin=subprocess.PIPE,stdout=subprocess.PIPE)

    inputdata= seq +"\n" + "A" #I need to use a 1 base dummy sequence. Hopefully this has no effect
    result_rnafold = p1.communicate(input=inputdata)[0]
    result_lines = result_rnafold.splitlines()
    db = result_lines[2]
    stru = ParseDotBracket(db)
    freeEnergy = result_lines[6]
    return [(float(freeEnergy),stru,db)]
      
#connectedStructure is of the form [strand1, strand2, etc] where strandN is of the form [(connectedStrandId,connectedBaseId),(connectedStrandId,connectedBaseId),etc]
def ParseViennaResults(viennaResultLine):
    line_split = viennaResultLine.split(' ')
    #print line_split
    db = line_split[0]
    if len(line_split) == 2: #Free energy is a negative value with double digits on LHS of comma
        freeEnergy = line_split[1].replace('(','').replace(')','')
    elif len(line_split) == 3: #It is a negative value with a single digit on LHS of comma
        freeEnergy = line_split[2].replace(')','')
    else: #It is single digit with no negative (e.g. 0)
        freeEnergy = line_split[3].replace(')','') 
    connectedStructure = ParseDotBracket(db)
    return [(float(freeEnergy),connectedStructure,db)] 

def ViennaFoldBatch(seqs):
    inputData = ''
    for seq in seqs:
        inputData = inputData+seq+"\n"
    inputData = inputData[:-1] #Get rid of the last newline

    p1 = subprocess.Popen(["C:\Vienna\RNAfold.exe"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)   
    result_rnafold_batch = p1.communicate(input=inputData)[0]
    return ParseViennaBatch(result_rnafold_batch)

def ViennaCoFoldBatch(seqsPairs):
    inputData = ''
    for pair in seqsPairs:
        inputData = inputData+pair[0]+"&"+pair[1] +"\n"
    inputData = inputData[:-1] #Get rid of the last newline

    p1 = subprocess.Popen(["C:\Vienna\RNAcofold.exe"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)   
    result_rnacofold_batch = p1.communicate(input=inputData)[0]
    return ParseViennaBatch(result_rnacofold_batch)

def ParseViennaBatch(batchResults):
    result_batch_split = batchResults.split('\n')
    results_batch = list()
    numSeqs = (len(result_batch_split) - 1)/2
    for i in range(numSeqs):
        results_batch.append(ParseViennaResults(result_batch_split[2*i+1]))
    return results_batch

def RactipBatch(seqsPairs):
    inputData = ''
    for pair in seqsPairs:
        inputData = inputData+pair[0]+"\n"+pair[1] +"\n"
    inputData = inputData[:-1] #Get rid of the last newline

    p1 = subprocess.Popen(["C:/ractip/ractipBatch.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e","--acc-num=0","--min-w=0","--acc-max-ss","--max-w=100","-t0.1","-a50","-u0.2","-b0"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    #p1 = subprocess.Popen(["C:/ractip/ractipBatch.exe", "C:/ractip/NOTLigand.fasta", "C:/ractip/NOTGate.fasta","-e"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)


    result_ractip_batch = p1.communicate(input=inputData)[0]
    #print result_ractip_batch
    result_lines = result_ractip_batch.splitlines()
    #print result_lines
    numPairs = len(result_lines)/7
    #print numPairs

    results = list()
    for pairId in range(numPairs):
        db1 = result_lines[pairId*7+2]
        db2 = result_lines[pairId*7+5]
        dbDimer = db1 + "&" + db2
        struDimer = ParseDotBracket(dbDimer)
        freeEnergy = result_lines[pairId*7+6]
        results.append([(float(freeEnergy),struDimer,dbDimer)]) #Added the list brackets to make it consisent with RNAStructure which returns a list of conformations
    return results

def ViennaDistanceBatch(structurePairs):
    inputData = ''
    for pair in structurePairs:
        inputData = inputData + pair[0]+ '\n' + pair[1] + '\n'
    #print inputData
    p1 = subprocess.Popen(["C:\Vienna\RNAdistance.exe"],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    output = p1.communicate(input=inputData)[0]
    lineSplit = output.split('/n')
    spaceSplit = list()
    result_rnadistance = list()
    for line in lineSplit:
        spaceSplit += line.split(' ')
    for i in range(len(structurePairs)):
        result_rnadistance.append(float(spaceSplit[1+i*3]))

    return result_rnadistance

#def RactipMulticore(dTasksRactip,foldingParams):
def RactipMulticore(dTasksRactip):
    foldingParams = [50,0,0.1,0.2,0]
    ractipDResults = list()
    for dTaskRactip in dTasksRactip:
        result = Ractip(dTaskRactip[1][0],dTaskRactip[1][1],foldingParams)
        ractipDResults.append(result)
    return ractipDResults

def RactipMulticoreSingle(mTasksRactip):
    foldingParams = [50,0,0.1,0.2,0]
    ractipMResults = list()
    for mTaskRactip in mTasksRactip:
        result = RactipSingle(mTaskRactip[1],foldingParams)
        ractipMResults.append(result)
    return ractipMResults

def Multicore(tasks,function,poolSize):
    results = list()
    n = len(tasks)/poolSize
    tasksMulticore = [tasks[i:i + n] for i in xrange(0, len(tasks), n)]
    pool = Pool(poolSize) 
    pool_outputs = pool.map(function, tasksMulticore)
    for id,output in enumerate(pool_outputs): 
        results += output
    pool.close()
    pool.join()

    return results
#structurePairs = [('(((.)))..','(((.)))..'),('(((.)))..','.......'),('((()))(((.)))..','(((......)))..')]
#print ViennaDistanceBatch(structurePairs)



seq1 = 'AAACUUGGAUAGGGAUGAACUA'
#seq1 = 'GGGCGACCCUGAUGAGCUUGAGUUUUAGUUCAUCCCUAUCCAGGUUUAUCAGGCGAAACGGUGAAAGCCGUAGGUUGCCC'
seq2 = 'GGGCGACCCUGAUGAGCUUGAGUUUUAGUUCAUCCCUAUCCAGGUUUAUCAGGCGAAACGGUGAAAGCCGUAGGUUGCCC'
seqs = 5000*[seq2]
seqsPairs = 1*[(seq1,seq2)]

'''
seqsPairs = 1*[(seq1,seq2)]
ractipBatchResults = RactipBatch(seqsPairs)
#print ractipBatchResults
print "Finished"


seqsPairs = 500*[(seq1,seq2)]
ractipResults = list()
for seqPair in seqsPairs:
    ractipResults.append(Ractip(seqPair[0],seqPair[1],[50,0,0.1,0.2,0]))
#print ractipResults
print "Finished"
'''

'''
#viennaBatchResults = ViennaCoFoldBatch(seqsPairs)
#for viennaResult in viennaBatchResults:
#    print viennaResult[0][0]
#print Ractip(seq,seq2)


print "Vienna Fold"
for i in range(5000):
    ViennaFold(seq2)


print "Vienna CoFold"
for i in range(1000):
    print ViennaCoFold(seq1,seq2)[0][0]
  
print "RNAStructure Fold"
print SingleFold(seq1)[0][0]

print "RNAStructure CoFold"
print BiFold(seq1,seq2,1)[0][0]
print "Ractip CoFold"
print Ractip(seq1,seq2)[0][0]


structure1 = '(((((.....)))))'
structure2 = '...((.....))...'
print ViennaDistance(structure1,structure2)


structure = '(((((((.....)))))...((('
print structure
print GetRzPartOfDimer(structure)
'''