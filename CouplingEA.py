import sys
sys.path.insert(0, 'C:\Users\Nick\Documents\Trading')
from EAlib import *
import numpy as np
from collections import defaultdict
import itertools
import math
sys.path.insert(0, 'C:\Users\Nick\Documents\Visual Studio 2015\Projects\EAserver')
from flk import InsertMetaResults, RetrieveTrialId, InsertTrial
import RNAEA

class CouplingInd(individual):
    def __init__(self):
        individual.__init__(self) 
        self.potentialFitnesses = dict()
        self.evolvedSeqs = dict()
        self.ampResults  = None
        self.convResults = None
        self.ampTrialId  = -1
        self.convTrialId = -1
        
        evolvedNames = ['Inner','Outer']
        signalLength = 22
        #Random initialization
        for evolvedName in evolvedNames:
            evolvedType = evolvedName[0]
            self.evolvedSeqs[evolvedName] = self.RandomSeq(signalLength)

    def RandomSeq(self,seqLength):
        seq = list()
        for baseId in range(seqLength):
            seq.append(np.random.randint(0,4))
        return seq
                             
 

class CouplingEA(EA):
    def __init__(self,instanceEAParams,foldingParams):
        EA.__init__(self)
        self.instanceEAParams = instanceEAParams
        self.foldingParams    = foldingParams
        print self.instanceEAParams
 

    def create_individual(self):
        newIndividual = CouplingInd()
        return newIndividual

    def configure_population(self): #This one should always be passed
        pass

    def generate_semantics_offspring(self):
        for ind in self.offspring:
            couplingsAmp         =  {("Input",1,0,0) : ind.evolvedSeqs['Outer'], ("Input",1,1,0) : ind.evolvedSeqs['Inner']}
            amplifierEAInstance  = RNAEA.RNAEA('Amplifier',[couplingsAmp], self.foldingParams)
            InsertTrial(self.instanceEAParams+(amplifierEAInstance.colorStrings,'Amplifier',1)) #meta level is 1
            ind.ampTrialId = RetrieveTrialId()
            print "Starting amplifier trial with id: " + str(ind.ampTrialId)
            ind.amplifierResults = amplifierEAInstance.run(*self.instanceEAParams)[0] #take the first element of the fittest front

            couplingsConv        =  {("Input",7,7,7) : ind.evolvedSeqs['Inner'], ("Input",8,8,8) : ind.evolvedSeqs['Outer']}
            converterEAInstance  = RNAEA.RNAEA('Converter',[couplingsConv], self.foldingParams)
            InsertTrial(self.instanceEAParams+(converterEAInstance.colorStrings,'Converter',1))
            ind.convTrialId = RetrieveTrialId()
            print "Starting converter trial with id: " + str(ind.convTrialId)
            ind.converterResults = converterEAInstance.run(*self.instanceEAParams)[0] 

    def InsertGenoPheno(self,popType):

        rows = list()
        if   popType == 0: indsToInsert = self.offspring
        elif popType == 1: indsToInsert = self.population
        elif popType == 2: indsToInsert = self.archive
        for ind in indsToInsert:
            rows.append((ind.id,ind.ampTrialId,ind.convTrialId,self.trialId))
        
        InsertMetaResults(rows)

    def evaluate_fitness(self,ind): 
        ind.potentialFitnesses['placeholder'] = -1        

    def mutate_parent(self,ind):
        seqNames = ind.evolvedSeqs.keys()
        for mutationId in range(self.mutation_rate):            
            sampleSeqId = np.random.randint(0,len(ind.evolvedSeqs))
            seqToMutateName = seqNames[sampleSeqId]

            sampleEntry = np.random.randint(0,len(ind.evolvedSeqs[seqToMutateName]))

            ind.evolvedSeqs[seqToMutateName][sampleEntry] = np.random.randint(0,4) # 'bit' flip mutation